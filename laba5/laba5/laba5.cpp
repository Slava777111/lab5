#include "stdafx.h"
#include <stdio.h>
#include <omp.h>
#include <iostream>

void main(int  argc, char*argv[])
{
	int rank, size;
#pragma omp parallel num_threads(4)
	{
		rank = omp_get_thread_num();
		size = omp_get_num_threads();
		printf("Hello world from process %d of %d\n", rank, size);
	}
	system("pause");

}

